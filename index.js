// alert("Hello World!");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let promptName = alert("Who dares disturb my slumber?!");
console.log("My apologies, master. Welcome to my humble page.");
function userInformation() {

	let userFullName = prompt("May I know your Full Name, my Liege?");
	let userAge = prompt("Your age?");
	let userLocation = prompt("To where are your castle located, dear Sire?");

	console.log("Welcome to the city of Pages "+userFullName);
	console.log("How young to be a knight at age of "+userAge);
	console.log("Must be lovely at "+userLocation);
	alert("Thank you for your compliance, your Highness.")
}
userInformation();

function functionFiveFavBands() {
	let nameOfBands = ["Ben&Ben", "Scorpions", "Beegees", "Air Supply", "David Cook"]
	console.log("1. "+nameOfBands[0]);
	console.log("2. "+nameOfBands[1]);
	console.log("3. "+nameOfBands[2]);
	console.log("4. "+nameOfBands[3]);
	console.log("5. "+nameOfBands[4]);
}
functionFiveFavBands();

function functionFiveFavMovies() {
	function funcFirstMovie(){
		let firstMovie = "Hercules";
		console.log("1. "+firstMovie);
		console.log("Rotten Tomotoes Reating: 80%");
	}
	funcFirstMovie();

	function funcSecondMovie(){
		let secondMovie = "Pursuit of Happyness";
		console.log("2. "+secondMovie);
		console.log("Rotten Tomotoes Reating: 90%");
	}
	funcSecondMovie();

	function funcThirdMovie(){
		let thirdMovie = "Forest Gump";
		console.log("3. "+thirdMovie);
		console.log("Rotten Tomotoes Reating: 88%");
	}
	funcThirdMovie();

	function funcFourthMovie(){
		let fourthMovie = "Ready Player One";
		console.log("4. "+fourthMovie);
		console.log("Rotten Tomotoes Reating: 89%");
	}
	funcFourthMovie();

	function funcFifthMovie(){
		let fifthMovie = "Ano Hana";
		console.log("5. "+fifthMovie);
		console.log("Rotten Tomotoes Reating: 96%");
	}
	funcFifthMovie()
}
functionFiveFavMovies();

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
/*console.log(friend1);
console.log(friend2);*/